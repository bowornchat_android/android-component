package buu.bowornchat.kotlinandroidtutorial

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class ShowNameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_name)
        val name:String = intent.getStringExtra("name")?:"unknown"
        val txtName = findViewById<TextView>(R.id.txtname)
        txtName.text = name
    }
}